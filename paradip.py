



from bs4 import BeautifulSoup
import os
import requests
import datetime
import time
from projpaths import proj_path




def ind_file(url):
    
    page = requests.get(url)
    soup  =BeautifulSoup(page.text,'lxml')
    l = soup.find_all('table' , attrs={'class':'table-one mb'})
    l = l[0]
    l = l.find_all('tr')
    l = l [1]
    l = l.find_all('td')
    i_ = l[2]
    date = l[1].text
    link_here = i_.find_all('a')[0]['href']
    full_link = 'https://paradipport.gov.in/' + link_here
    response = requests.get(full_link)
    
    ''' required paths '''
    
    ind_port_folder_name = 'ind_port_data'
    port_name = 'PARADIP'

    
    port_path = os.path.join(proj_path,ind_port_folder_name)
    
    if not os.path.exists(port_path):
        os.mkdir(port_path)
    nameportpath = os.path.join(port_path , port_name)
    if not os.path.exists(nameportpath):
        os.mkdir(nameportpath)
    
    today_ = str(datetime.date.today())
    date = date.replace('/','-').replace(' ','')
    date = today_+'-'+date
    
    with open(os.path.join(nameportpath, date+'.pdf' )  , 'wb') as fd:
        fd.write(response.content)
    time.sleep(5)
 
    
def main():
    urls =  ['https://paradipport.gov.in/Daily_Traffic_Update.aspx',
             'https://paradipport.gov.in/DailyStockUpdate.aspx',
             'https://paradipport.gov.in/MonthlyTraffic.aspx']
    for i in urls:
        print(i)
        ind_file(i)
        
    
if __name__=='__main__':
    main()
   
