# -*- coding: utf-8 -*-

import chennai,jnpct,kolkata,mangalore,mumbaiport,paradip
import logging
import datetime
from projpaths import proj_path
import os

def main():
    l  = [chennai,jnpct,kolkata,mangalore,mumbaiport,paradip]

    log_file = os.path.join(proj_path , 'indports.log')
    logging.basicConfig(filename=log_file,level=logging.INFO)
    today_ = datetime.date.today()
    logging.info(today_)

    for i in l:
        try:
            i.main()
            str_here = str(i) + ' done'
            logging.info(str_here)

        except:
            str_here = str(i) +  ' skipped'
            logging.info(str_here)


if __name__=='__main__':
    main()
