#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 14:17:20 2019

@author: nithishreddy
"""

import os
import requests
import datetime
from lxml import html
import time
from projpaths import proj_path



def main():
    url = 'http://mumbaiport.gov.in/index3_n.asp?sslid=75&subsublinkid=761&langid=1'
    
    page = requests.get(url)
    
    tree = html.fromstring(page.content)
    
    
    l = tree.xpath('//a[@target="_blank"]')
    
    l = [i.get('href') for i in l]
    
    main_url = "http://mumbaiport.gov.in/"
    
    l = [main_url+i for i in l]
    
    
    
#    proj_path = '/Users/nithishreddy/workspace/python/port_data'
    ind_port_folder_name = 'ind_port_data'
    port_name = 'MUMBAIPORT'
    
    
    port_path = os.path.join(proj_path,ind_port_folder_name)
    
    if not os.path.exists(port_path):
        os.mkdir(port_path)
    nameportpath = os.path.join(port_path , port_name)
    if not os.path.exists(nameportpath):
        os.mkdir(nameportpath)
    
    
    
    today_ = str(datetime.date.today())
    
    l = [i for i in l if i.endswith('.pdf')]
    for i in l:
#        print(i)
        date = i
        date = date.replace('/','-').replace(' ','')
        response = requests.get(i)
        date = today_+'-'+date
        with open(os.path.join(nameportpath, date )  , 'wb') as fd:
            fd.write(response.content)
        time.sleep(3)


if __name__=='__main__':
    main()