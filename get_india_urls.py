from projpaths import proj_path
import os

with open(os.path.join(proj_path , 'indiaalllinks.txt'),'r') as f:
    l = f.readlines()
    
l = [i.strip() for i in l]

marine_urls = list(set(l))
marine_urls = sorted(marine_urls)

marine_url_names = [i.split('|')[-2] for i in marine_urls]
