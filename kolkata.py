# -*- coding: utf-8 -*-


import os
import requests
import datetime
from lxml import html
import time
from projpaths import proj_path,CHROMEDRIVER_PATH

from bs4 import BeautifulSoup
import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

#options = Options()
#options.add_experimental_option("prefs", {
#  "download.default_directory": r"/Users/nithishreddy/workspace/python/port_data/ind_port_data/KOLKATAPORT/",
#  "download.prompt_for_download": False,
#  "download.directory_upgrade": True,
#  "safebrowsing.enabled": True
#})

#driver = webdriver.Chrome(CHROMEDRIVER_PATH , options=options)

def main():
    
    url = "http://www.kolkataporttrust.gov.in/index1.php?layout=3&lang=1&level=2&sublinkid=799&lid=711"
    
    page = requests.get(url)
        
    tree = html.fromstring(page.content)
    
    
    download_links = []
    
    
    link1 = tree.xpath('//a[@title="Expected Vessel(s) Arrival Schedule"]')[0].get('href')
    
    link2 = tree.xpath('//a[@title="Expected Vessel(s) Departure Schedule"]')[0].get('href')
    
    download_links.append(link1)
    download_links.append(link2)
    
    main_url = "http://www.kolkataporttrust.gov.in/"
    
    download_links = [main_url+i for i in download_links]
    
    #report_view.php?view_req=1&layout=3&lang=1&level=0&linkid=&rid=32
    
    
#    proj_path = '/Users/nithishreddy/workspace/python/port_data'
    ind_port_folder_name = 'ind_port_data'
    port_name = 'KOLKATAPORT'
        
    port_path = os.path.join(proj_path,ind_port_folder_name)
        
    if not os.path.exists(port_path):
        os.mkdir(port_path)
    nameportpath = os.path.join(port_path , port_name)
    if not os.path.exists(nameportpath):
        os.mkdir(nameportpath)
        
    def enable_download_in_headless_chrome( driver, download_dir):
        # add missing support for chrome "send_command"  to selenium webdriver
        driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    
        params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
        command_result = driver.execute("send_command", params)
    
    
    options = Options()
#    options.add_experimental_option("prefs", {
#      "download.default_directory": nameportpath,
#      "download.prompt_for_download": False,
#      "download.directory_upgrade": True,
#      "safebrowsing.enabled": True
#    })
    options.add_argument('--headless')
#    options.add_argument('--disable-gpu')
#    options.add_argument('--window-size=1200,1100')

        
    driver = webdriver.Chrome(CHROMEDRIVER_PATH , options=options)

#    today_ = str(datetime.date.today())
    enable_download_in_headless_chrome(driver,  nameportpath)
    
    for num_ , i in enumerate(download_links):
#        date = i
#        date = date.replace('/','-').replace(' ','')
#        response = requests.get(i)
        driver.get(i)
        
#        elem = driver.find_element_by_xpath('//img[@src="images/report_excel.png"]')
        elem  = driver.find_element_by_id('maincont')
        elem = elem.get_attribute('innerHTML')
        
        elem = BeautifulSoup(elem,'lxml')
        l = elem.find_all('tbody')
        l = l[0]
        l = l.find_all('tr')
        l = l[1:]
        l = [i.find_all('td') for i in l ]
        l = [ [j.text for j in i] for i in l ]        
        df = pd.DataFrame(l) 
        
        df.to_csv(os.path.join( nameportpath , str(datetime.date.today())+'_' +str(num_)+'.csv' ) )
        
        time.sleep(5)
    
#    files = os.listdir(nameportpath)
#    files = [i.replace('/','').replace(' ','') for i in files]
#    new_files= [str(datetime.date.today())+'_'+i for i in files]
#    
#    for old,new in zip(files , new_files):
#        old_path = os.path.join(nameportpath , old)
#        new_path = os.path.join(nameportpath , new)
#        os.rename(old_path , new_path)
    
    
if __name__=='__main__':
    main()
