import requests
import sys
import os
import json
import time
import random
import datetime
from projpaths import proj_path
import logging



#lines = [ x.strip() for x in open(sys.argv[1]).readlines() ]




def fetch_all(sid,count):
    base_url = 'https://mmb.moneycontrol.com/index.php?q=topic/ajax_call&section=get_messages&is_topic_page=1&offset=&lmid=&isp=0&gmt=tp_lm&tid={0}&pgno=1'
    urlx = 'https://mmb.moneycontrol.com/index.php?q=topic/ajax_call&section=get_messages&is_topic_page=1&offset={0}&lmid={1}&isp=1&gmt=tp_lm&tid={2}&pgno={3}'

    main_url_page = base_url.format(sid)
    firstpage = requests.get(main_url_page).text

    outdirname = 'mc_user_data'

    outdir = os.path.join(proj_path , outdirname)
    if not os.path.exists(outdir):
        os.mkdir(outdir)

#    outdir = '/Users/nithishreddy/workspace/python/port_data/mc_user_data'

    today_fold = str(datetime.date.today())

    outdir = os.path.join(outdir,today_fold)

    if not os.path.exists(outdir):
        os.mkdir(outdir)


    fname = os.path.join(outdir, "{0}_page_{1}.json".format(sid,1)  )

    listdicts = page_to_dicts(firstpage)

    with open(fname,'w') as outfile:
        json.dump(listdicts,outfile)

    prevpage = json.loads(firstpage)

    print("main url {0}".format(main_url_page))

    page = 2
    offset = 10
    msg_id = prevpage[-1]['msg_id']

    while True:
        url = urlx.format( offset, msg_id, sid,page )
        paget = requests.get(url).text
        fname = os.path.join(outdir,"{0}_page_{1}.json".format(sid , page))
        print( "page {0}".format(page))
        listdicts = page_to_dicts(paget)

        with open(fname,'w') as outfile:
            json.dump(listdicts,outfile)

        prevpage = json.loads(paget)
        msg_id = prevpage[-1]['msg_id']
        offset += 10
        page += 1
        #count -= 10
        if offset/count > 0.5:
            print('near the break')
            break
        time.sleep(5.0*random.random())
    return




def remove_quotes(s):
    s = s.replace("'" , "")
    s = s.replace('"' , '')
    return s





def msg_to_dict(x):

    d_  = {}
    for i in x.split(','):
        i_l = i.split(':')
        if len(i_l)==2:
            key,value= i_l
            key=remove_quotes(key) ; value = remove_quotes(value)
            d_[key] = value
        elif len(i_l)>2:
            key = i_l[0]
            value = ':'.join(i_l[1:])
            key=remove_quotes(key) ; value = remove_quotes(value)
            d_[key] = value
    return d_


def page_to_dicts(paget):

    l = paget
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(l,'lxml')
    l = soup.find_all('p')
    l = l[0]
    l = l.text
    l = l.split("msg_id")
    l = l[1:]
    l = [msg_to_dict(i) for i in l]
    return l



#with open(os.path.join(outdir ,'406_page_5.json' ) , 'r') as f:
#    xx = json.load(f)



def main():

    # read from sorted
    log_file = os.path.join(proj_path , 'mcuserdata.log')
    logging.basicConfig(filename=log_file,level=logging.INFO)
    today_ = datetime.date.today()
    logging.info(today_)
    del today_

    with open(os.path.join(proj_path , 'sorted'),'r') as f:
        lines = f.readlines()

    total_len = len(lines)

    for num_,line in enumerate(lines):
    #    line = lines[1]
        tokens = line.split(',')
        company_name = tokens[0]
        logging.info("company name {0} started".format(company_name))
        logging.info("company {} of {}".format(num_ , total_len))
        try:
            fetch_all(sid = tokens[1],count = int(tokens[2]) )
#            logging.info("scrapping for {0} done".format(company_name))
        except Exception as e:
#            print('near Exception')
            logging.info(e)
            pass



if __name__=='__main__':
    main()



