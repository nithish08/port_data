#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 18:01:22 2019

@author: nithishreddy
"""

from bs4 import BeautifulSoup
import pandas as pd
from selenium import webdriver
import time
from get_india_urls import marine_urls as marinetrafficurls 
from get_india_urls import marine_url_names as marineportnames
import datetime
import os
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

import pickle
#import selenium.webdriver 


all_paths = ['//*[@id="borderLayout_eGridPanel"]/div[1]/div/div/div[3]/div[1]/div/div[{0}]/div[3]/div/div/a'.format(str(i)) for i in range(1,21)]



#for i in all_paths[2:]:
#    try:
url = "https://www.marinetraffic.com/en/data/?asset_type=ports&columns=flag,portname,unlocode,photo,vessels_in_port,vessels_departures,vessels_arrivals,vessels_expected_arrivals,local_time,anchorage,geographical_area_one,geographical_area_two,coverage&flag_in|in|India|flag_in=IN"

CHROMEDRIVER_PATH = '/Users/nithishreddy/chromedriver'
options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
options.add_argument('--window-size=1200,1100')



cookies = pickle.load(open("cookies.pkl", "rb"))
for cookie in cookies:
    driver.add_cookie(cookie)
    
    
    
def remove_ads(browser):
    all_iframes = browser.find_elements_by_tag_name("iframe")
    if len(all_iframes) > 0:
        print("Ad Found\n")
        browser.execute_script("""
            var elems = document.getElementsByTagName("iframe"); 
            for(var i = 0, max = elems.length; i < max; i++)
                 {
                     elems[i].hidden=true;
                 }
                              """)
        print('Total Ads: ' + str(len(all_iframes)))
    else:
        print('No frames found')
        

for page_num in range(1):

    for i in all_paths:
        try:
            driver = webdriver.Chrome(CHROMEDRIVER_PATH,options=options)
            driver.get(url)
            time.sleep(10)
            
            inp_ = '//*[@id="reporting_ag_grid"]/div/div[2]/div[3]/div/div/div/div/div[3]/div/div/div/input'
            inp_ = driver.find_element_by_xpath(inp_)
            inp_.send_keys(Keys.BACKSPACE)
            inp_.send_keys(page_num)
            
            time.sleep(3)
            
        #    next_button.click()
            path1 = i
            
            elem1 = driver.find_element_by_xpath(path1)
            
            elem1 = elem1.get_attribute('href')
            
            driver.get(elem1)
            
            time.sleep(5)
            
            remove_ads(driver)
            
            try:
                rem = '//*[@id="leadinModal-397174"]/div[2]/button'
                rem = driver.find_element_by_xpath(rem)
                rem.click()
            except:
                pass
            
            time.sleep(2)
            
            path2 = '//*[@id="detail-wigdet-tabs"]/li[2]/a/span'
            elem2 = driver.find_element_by_xpath(path2)
            
            elem2.click()
            
            time.sleep(1)
            
            path3 = '//*[@id="tabs-arr-dep"]/div[3]/a'
            elem3 = driver.find_element_by_xpath(path3)
            elem3 = elem3.get_attribute('href')
            
            print(elem3)
            with open('indiaalllinks.txt','a') as f:
                f.write(elem3)
                f.write('\n')
                
            driver.close()
            
        
    
        except:
            print('skipped')
            continue
        
    print("page num {0} done".format(page_num))



time.sleep(10)
final_link = driver.current_url

#    except:
#        driver.close()
#        print(i , 'skipped')
#        continue

inp_ = '//*[@id="reporting_ag_grid"]/div/div[2]/div[3]/div/div/div/div/div[3]/div/div/div/input'

inp_ = driver.find_element_by_xpath(inp_)
inp_.send_


#driver = selenium.webdriver.Firefox()
#driver.get("http://www.google.com")

pickle.dump( driver.get_cookies() , open("cookies.pkl","wb"))




