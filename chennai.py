#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 16:02:26 2019

@author: nithishreddy
"""


import os
import requests
import datetime
from lxml import html
import time
from projpaths import proj_path,CHROMEDRIVER_PATH
#from selenium.webdriver.common.keys import Keys


from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def main():
    url = "https://www.chennaiport.gov.in/"
    
    page = requests.get(url)
        
    tree = html.fromstring(page.content)
    
    download_links = []
    
    e = tree.xpath("//a[@title='Daily Vessels Position Report']")
    e = e[0]
    download_links.append(e.get('href'))
    
    
    
    e = tree.xpath("//a[@title='Daily Expected Vessels Report']")
    e = e[0]
    download_links.append(e.get('href'))
    
    
#    proj_path = proj_path
    ind_port_folder_name = 'ind_port_data'
    port_name = 'CHENNAIPORT'
        
    port_path = os.path.join(proj_path,ind_port_folder_name)
        
    if not os.path.exists(port_path):
        os.mkdir(port_path)
    nameportpath = os.path.join(port_path , port_name)
    if not os.path.exists(nameportpath):
        os.mkdir(nameportpath)
        
        
    options = Options()
#    options.add_experimental_option("prefs", {
#      "download.default_directory": nameportpath,
#      "download.prompt_for_download": False,
#      "download.directory_upgrade": True,
#      "safebrowsing.enabled": True
#    })
    options.add_argument('--headless')
#    options.add_argument('--disable-gpu')
#    options.add_argument('--window-size=1200,1100')
    
    def enable_download_in_headless_chrome( driver, download_dir):
        # add missing support for chrome "send_command"  to selenium webdriver
        driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    
        params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
        command_result = driver.execute("send_command", params)
    
    
    driver = webdriver.Chrome(CHROMEDRIVER_PATH,options = options)
        
    today_ = str(datetime.date.today())
    enable_download_in_headless_chrome(driver,  nameportpath)
    
    for i in download_links:
        date = i
        date = date.replace('/','-').replace(' ','')
#        response = requests.get(i)
        driver.get(i)
#        driver.find_element_by_xpath('//img[@src="images/report_excel.png"]').click()
#        page_src = driver.page_source.encode("utf-8")

        date = today_+'-'+date
        
#        with open(os.path.join(nameportpath, date +'.html')  , 'wb') as fd:
#            fd.write(page_src)
        
        time.sleep(3)
        
    file_name = os.path.join(nameportpath,'eta.pdf')
    new_file_name = os.path.join(nameportpath,str(datetime.date.today())+'_eta_file.pdf' )
    os.rename(file_name , new_file_name)
    
    file_name = os.path.join(nameportpath,'pos.pdf')
    new_file_name = os.path.join(nameportpath,str(datetime.date.today())+'_pos_file.pdf' )
    os.rename(file_name , new_file_name)
    
    

if __name__=='__main__':
    main()