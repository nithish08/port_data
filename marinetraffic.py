# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
import pandas as pd
from selenium import webdriver
import time
from get_india_urls import marine_urls as marinetrafficurls
from get_india_urls import marine_url_names as marineportnames
import datetime
import os
from selenium.webdriver.chrome.options import Options
#from selenium.webdriver.common.keys import Keys
from projpaths import proj_path,CHROMEDRIVER_PATH
import logging

def give_table_data(driver):
    e = driver.find_element_by_xpath(
        "//div[@class='ag-pinned-left-cols-viewport']")
    soup = BeautifulSoup(e.get_attribute('innerHTML'), 'html.parser')
    comps = [i.text for i in soup.find_all(
        "a", attrs={"class": 'ag-cell-content-link'})]
    e = driver.find_element_by_xpath(
        "//div[@class='ag-body-viewport-wrapper']")
    soup = BeautifulSoup(e.get_attribute('innerHTML'), 'html.parser')
    rows = soup.find_all("div", attrs={"role": "row"})

    def give_rows_data(rows):
        l = []
        for row in rows:
            row = [i.text for i in row.find_all(
                "div", attrs={"class": "ag-react-container"})]
            l.append(row)
        return l
    l_ = give_rows_data(rows)
    l_ = pd.DataFrame(l_)
    l_.columns = ['Port Call Type', 'Port Type', 'Current Port', 'Ata/atd',
                  'Voyage origin port', 'Leg start Port/anch', 'In Transit Port calls']

    comps = pd.DataFrame(comps, columns=['Vessel Name'])
    data = pd.concat([comps, l_], axis=1)

    return data


def return_company_df(driver):

    x = driver.find_elements_by_xpath(
        "//div[contains(@class,'e-paginationActionsFieldWrapper')]")
    max_pages = int(BeautifulSoup(
        x[0].get_attribute('innerHTML')).text.split()[-1])
#    print(max_pages)
    dfs = []
    df = give_table_data(driver)
    dfs.append(df)
#    print('initital data pulled')
    if max_pages > 1:
        for page_num in range(max_pages-1):
            #            print('page num {0}'.format(page_num+2))
            #            val = driver.find_element_by_xpath("//input[@aria-invalid='false'][@min='1']").get_attribute("value")
            #        driver.find_element_by_xpath("//input[@aria-invalid='false'][@min='1']").click()
            driver.find_element_by_xpath(
                "//*[@id='reporting_ag_grid']/div/div[2]/div[3]/div/div/div/div/div[3]/button[2]").click()
            time.sleep(3)
            df = give_table_data(driver)
            dfs.append(df)
            print(df.shape)

    dfs = pd.concat(dfs)
    print('dfs shape', dfs.shape, dfs.drop_duplicates().shape)
    return dfs


def main():

#    path = '/home/nithish/workspace/python/port_data/marinetraffic_data'

    path = os.path.join(proj_path,'marinetraffic_data')
    if not os.path.exists(path):
        os.mkdir(path)
    log_file = os.path.join(proj_path , 'marinetraffic.log')
    logging.basicConfig(filename=log_file,level=logging.INFO)
    today_ = datetime.date.today()
    logging.info(today_)
#    logger.write(str(datetime.date.today( ) ) )
    # chrome headless
#    CHROMEDRIVER_PATH = '/Users/nithishreddy/chromedriver'
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--window-size=1200,1100')
    print('len of marineportnames' , len(marineportnames))

    for port_name, port_url in zip(marineportnames, marinetrafficurls):

        for attempt_ in range(1, 4):
            try:
                str_here = 'pulling data for {0} attempt {1}'.format(
                    port_name, attempt_)
                logging.info(str_here)

                port_path = os.path.join(path, port_name)
                if not os.path.exists(port_path):
                    os.mkdir(port_path)

                quote_page = port_url

                driver = webdriver.Chrome(CHROMEDRIVER_PATH, options=options)
                driver.get(quote_page)

                time.sleep(10)

                driver.find_element_by_id('guest-logggin').click()

                time.sleep(3)
                but_ = driver.find_element_by_id('email')
                but_.send_keys("nithish.bhu@gmail.com")
                driver.find_element_by_id(
                    'password').send_keys('marinetraffic123')
                driver.find_element_by_id('remembermeCheckbox').click()
                driver.find_element_by_id('login_form_submit').click()
                time.sleep(10)
                df = return_company_df(driver)
                df.to_csv(os.path.join(
                    port_path, port_name+'_' + str(today_)+'.csv'))
                time.sleep(3)

                driver.close()
                str_here = 'attempt {0} for {1} successful'.format(
                    attempt_, port_name)
#                logger.write(str_here + '\n')
                logging.info(str_here)

            except:
                continue

            break
#    logger.close()


if __name__ == '__main__':
    main()
