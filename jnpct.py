#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 13:27:23 2019

@author: nithishreddy
"""



from bs4 import BeautifulSoup
import os
import requests
import datetime
import time
from projpaths import proj_path


def main():
    url = 'http://jnport.gov.in/vessel.aspx'
    page = requests.get(url)
    soup  = BeautifulSoup(page.text,'lxml')
    l = soup.find_all('div' , attrs={'class':'main_inner'})
    l = l[0]
    l = l.find_all('li')
    l = [i.find('a')['href'] for i in l]
    
#    proj_path = '/Users/nithishreddy/workspace/python/port_data'
    ind_port_folder_name = 'ind_port_data'
    port_name = 'JNPCT'
    
    
    port_path = os.path.join(proj_path,ind_port_folder_name)
    
    if not os.path.exists(port_path):
        os.mkdir(port_path)
    nameportpath = os.path.join(port_path , port_name)
    if not os.path.exists(nameportpath):
        os.mkdir(nameportpath)
    
    today_ = str(datetime.date.today())
    
    for i in l:
        date = i
        date = date.replace('/','-').replace(' ','')
        
        response = requests.get(i)
        date = today_+'-'+date
        with open(os.path.join(nameportpath, date )  , 'wb') as fd:
            fd.write(response.content)
        time.sleep(3)

if __name__=='__main__':
    main()