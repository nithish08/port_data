#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 10:19:51 2019

@author: nithishreddy
"""
import sys


if sys.platform=='darwin':
    proj_path = '/Users/nithishreddy/workspace/python/port_data'
    CHROMEDRIVER_PATH = '/Users/nithishreddy/chromedriver'

elif sys.platform=='linux':
    proj_path = '/home/nithish/workspace/python/port_data'
    CHROMEDRIVER_PATH = '/home/nithish/chromedriver'