#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 17:01:00 2019

@author: nithishreddy
"""


import os
import requests
import datetime
#from lxml import html
import time
from projpaths import proj_path



def main():

    url_start = 'http://newmangaloreport.gov.in:8080/daily-vessels/english/'
    #day-month-year format indian format
    
    today = datetime.date.today()
    today = str(today.day)+'-'+str(today.month)+'-'+str(today.year)
    
    url = url_start+today+'.pdf'
    
    download_links = [url]
    
#    proj_path = '/Users/nithishreddy/workspace/python/port_data'
    ind_port_folder_name = 'ind_port_data'
    port_name = 'MANGALOREPORT'
        
    port_path = os.path.join(proj_path,ind_port_folder_name)
        
    if not os.path.exists(port_path):
        os.mkdir(port_path)
    nameportpath = os.path.join(port_path , port_name)
    if not os.path.exists(nameportpath):
        os.mkdir(nameportpath)
        
        
    today_ = str(datetime.date.today())
        
    for i in download_links:
        date = i
        date = date.replace('/','-').replace(' ','')
        response = requests.get(i)
        date = today_+'-'+date
        with open(os.path.join(nameportpath, date )  , 'wb') as fd:
            fd.write(response.content)
        time.sleep(3)
        
    
if __name__=='__main__':
    main()

