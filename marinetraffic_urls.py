# -*- coding: utf-8 -*-

marine_urls = ["https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|KANDLA|quicksearch_asset_id=Port-269",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&port_in|begins|PARADIP|port_in=17379",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|JAWAHARLAL%20NEHRU|quicksearch_asset_id=Port-2713",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|MUMBAI|quicksearch_asset_id=Port-2341",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|VIZAG|quicksearch_asset_id=Port-3048",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|CHENNAI|quicksearch_asset_id=Port-792",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|KOLKATA|quicksearch_asset_id=Port-2806",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|MANGALORE|quicksearch_asset_id=Port-17383",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|TUTICORIN|quicksearch_asset_id=Port-2986",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|ENNORE|quicksearch_asset_id=Port-791",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|KRISHNAPATNAM|quicksearch_asset_id=Port-1317",
 "https://www.marinetraffic.com/en/data/?asset_type=arrivals_departures&columns=shipname,move_type,port_type,port_name,ata_atd,origin_port_name,leg_start_port,intransit&quicksearch|begins|KOCHI|quicksearch_asset_id=Port-2990"
 ]

marine_url_names = [i.split('|')[-2] for i in marine_urls]